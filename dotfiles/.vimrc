


:syn on         " Syntax highlighting
":set spell      " Spell checking
set showmatch   " When a bracket is inserted, briefly jump to a matching one
set incsearch   " Incremental search


" --- Tab settings ---
set tabstop=4
set shiftwidth=4
" Expand tabs for Python coding only (C/C++ in Blender uses tabs)
" set expandtab
set smarttab


" ---- Indenting ----
set autoindent    " Auto indent
set smartindent   " Smart indent
set ci            " C/C++ indents
set cin


" --- Column/Row Stuff ---
"set cul                     " Highlight the current line
:set number relativenumber   " Show line numbers
":set nu rnu
":set lines=40 columns=120    " Window size
:set colorcolumn=120


" --- Extra functionality helpers ---
filetype plugin on
filetype indent on

" auto-complete
set ofu=syntaxcomplete#Complete

" --- Macros ---
let @c = 'I#j'
let @u = '0xj'

