--
-- kitty cool-retro-term emacs xscreensaver flameshot
-- qutebrowser firefox
-- cmus ranger ghc htop

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local vicious = require("vicious")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

awful.mouse.snap.client_enabled = false
awful.mouse.snap.edge_enabled = false


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end


-- }}}

-- {{{ Variable definitions
homedir = os.getenv("HOME")
beautiful.init(homedir .. "/.config/awesome/themes/default/theme.lua")


-- Floating window
fwindow = { floating = true,
    is_fixed = false,
    ontop = false,
    width = 1500,
    height = 800,
    placement = awful.placement.centered
    }

-- Volume control
local APW = require("apw/widget")

color_black  = "#111111"
color_grey   = "#272727"
color_stops  = { {0,   "#00ff00"},
                 {0.5, "#ffff00"},
                 {1,   "#ff0000"}}

color_cpu_graph = {type = "linear", from = {0, 20}, to = {0, 0},
                    stops = color_stops}
graph_width = 40
graph_step_width = 2

ramwidget = wibox.widget.progressbar()
ramwidget:set_width (graph_width)
ramwidget:set_background_color "#225555"
ramwidget:set_color "#00ffff"
vicious.register(ramwidget, vicious.widgets.mem, "$1", 3)

swapwidget = wibox.widget.progressbar()
swapwidget:set_width (graph_width)
swapwidget:set_background_color"#552222" 
swapwidget:set_color {type = "linear", from = {0, 0}, to = {40, 0},
                          stops = { {0,   "#990000"},
                                    {0.5, "#bb0000"},
                                    {1,   "#ff0000"}}}
vicious.register(swapwidget, vicious.widgets.mem, "$5", 3)

netDwidget = wibox.widget.graph()
netDwidget:set_background_color "#111111"
netDwidget:set_color "#00ff00"
netDwidget:set_step_width (graph_step_width)
vicious.register(netDwidget, vicious.widgets.net, "${enp3s0 down_kb}", 3)

netUwidget = wibox.widget.graph()
netUwidget:set_background_color "#111111"
netUwidget:set_color "#ff00ff"
netUwidget:set_step_width (graph_step_width)
vicious.register(netUwidget, vicious.widgets.net, "${enp3s0 up_kb}", 2)

cpus = awful.widget.graph()
cpus:set_width (graph_width)
cpus:set_step_width (graph_step_width)
cpus:set_background_color (color_black)
cpus:set_color (color_cpu_graph)
vicious.register(cpus, vicious.widgets.cpu, "$1", 3)

-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.max,
    awful.layout.suit.tile,
    awful.layout.suit.floating,

--    awful.layout.suit.tile.left,
--    awful.layout.suit.tile.bottom,
--    awful.layout.suit.tile.top,
--    awful.layout.suit.fair,
--    awful.layout.suit.fair.horizontal,
--    awful.layout.suit.spiral,
--    awful.layout.suit.spiral.dwindle,
--    awful.layout.suit.max.fullscreen,
--    awful.layout.suit.magnifier,
--    awful.layout.suit.corner.nw,
--    awful.layout.suit.corner.ne,
--    awful.layout.suit.corner.sw,
--    awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

--[[
--  {{{ Menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

mymainmenu = awful.menu({
    items = {
        { "awesome", myawesomemenu, beautiful.awesome_icon },
        { "run", function() menubar.show() end },
        { "kitty", terminal },
        { "emacs", function() awful.spawn("emacsclient -nc") end },
        { "ranger", function() awful.spawn(terminal .. " -T ranger -e ranger") end },
        { "firefox", function() awful.spawn("firefox") end }
    }
})

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })
--]]

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end)
--                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
--                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
   awful.button({ }, 1, function (c)
         client.focus = c
         c:raise()
   end),
   awful.button({ }, 3, function (c)
         if c.minimized == false then
            c.minimized = true
         else
            c.minimized = false
         end
   end),
   awful.button({ }, 2, function (c) c:kill()
end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
--    awful.tag( { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" },
--               s, awful.layout.layouts[1])
    awful.tag( { "일", "이", "삼", "사", "오", "육", "칠", "팔", "구", "십" },
               s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)


    -- Create the wibox
    s.mywibox = awful.wibar({ position = "bottom",
                              screen   = s,
                              visible  = false,
                              ontop    = false })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
            cpus,
            ramwidget,
            swapwidget,
            netUwidget,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            mykeyboardlayout,
            APW,
            mytextclock,
            s.mylayoutbox,
        },
    }
end)


-- }}}

-- {{{ Mouse bindings
--root.buttons(gears.table.join(
--    awful.button({ }, 3, function () mymainmenu:toggle() end),
--    awful.button({ }, 4, awful.tag.viewnext),
--    awful.button({ }, 5, awful.tag.viewprev)
--))
-- }}}


-- The following function can be bound to a key, and be used to resize a
-- client using the keyboard.
function resize(c)
  local grabber
  grabber = awful.keygrabber.run(function(mod, key, event)
    if event == "release" then return end

    if     key == 'Up'    then c:relative_move(0, 0, 0, 5)
    elseif key == 'Down'  then c:relative_move(0, 0, 0, -5)
    elseif key == 'Right' then c:relative_move(0, 0, 5, 0)
    elseif key == 'Left'  then c:relative_move(0, 0, -5, 0)
    else   awful.keygrabber.stop(grabber)
    end
  end)
end


-- {{{ Key bindings
globalkeys = gears.table.join(

    awful.key({ modkey,            }, "'", resize(c)),

    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "`", awful.tag.history.restore,
       {description = "go back", group = "tag"}),


    awful.key({ modkey,           }, "n",
        function()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "p",
        function()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "j",
        function()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),



    --awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              --{description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "n", function() awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "p", function() awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "n", function() awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "p", function() awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function() awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Shift"   }, "Return", function() awful.spawn("cool-retro-term -T crt") end),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Control", "Shift" }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function() awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function() awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function() awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function() awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function() awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function() awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),

    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "]",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- My shortcuts
    -- Client Keys
    -- Center floating client
    awful.key({ modkey, "Shift" }, "y", awful.placement.centered),


    -- VOLUME CONTROL
    -- APW
    awful.key({ modkey }, "KP_Add", APW.Up),
    awful.key({ modkey }, "KP_Subtract", APW.Down),
    awful.key({ modkey }, "KP_Multiply", APW.ToggleMute),
    -- ALSA
--    awful.key({ modkey }, "KP_Add",      function()   awful.spawn("amixer set Master 5%+ unmute") end),
--    awful.key({ modkey }, "KP_Subtract", function() awful.spawn("amixer set Master 5%- unmute") end),
--    awful.key({ modkey }, "KP_Multiply", function() awful.spawn("amixer set Master toggle") end),


    -- LOCK
--    awful.key({ modkey }, "Escape", function() awful.spawn.easy_async_with_shell("xscreensaver-command --lock") end),
    awful.key({ modkey }, "Escape", function() awful.spawn("xscreensaver-command -lock") end),

    awful.key({ modkey }, "e", function() awful.spawn("emacsclient -nc") end),
--    awful.key({ modkey }, "d", function() awful.spawn("krita") end),
    awful.key({ modkey }, "q", function() awful.spawn("qutebrowser") end),
    awful.key({ modkey }, "r", function() awful.spawn(terminal .. " -T ranger -e ranger") end),
    awful.key({ modkey }, "a", function() awful.spawn("cool-retro-term -T cmus -e cmus") end),
    awful.key({ modkey }, "g", function() awful.spawn("cool-retro-term -T ghci -e ghci") end),
    awful.key({ modkey }, "/", function() awful.spawn("cool-retro-term -T htop -e htop") end),
    awful.key({ modkey }, "w", function() awful.spawn("firefox") end),
    awful.key({ modkey, "Control" }, "z", function()
           myscreen = awful.screen.focused() myscreen.mywibox.visible = not myscreen.mywibox.visible  end,
              { description = "toggle statusbar" }),

    -- cmus-remote
    awful.key({ modkey }, "b", function() awful.spawn("cmus-remote --next") end),
    awful.key({ modkey }, "v", function() awful.spawn("cmus-remote --stop") end),
    awful.key({ modkey }, "c", function() awful.spawn("cmus-remote --pause") end),
    awful.key({ modkey }, "x", function() awful.spawn("cmus-remote --play") end),
    awful.key({ modkey }, "z", function() awful.spawn("cmus-remote --prev") end),

    -- Flameshot
    awful.key({ modkey }, "Print", function() awful.spawn("flameshot gui") end),
    awful.key({}, "Print", function() awful.spawn("flameshot full -p " .. homedir .. "/.screenshots") end),

    -- killall
    -- ghc
    awful.key({ modkey, "Control", "Shift" ,"Mod1" }, "h", function()
          awful.spawn.easy_async_with_shell("killall ghc") end),
    -- blender
    awful.key({ modkey, "Control", "Shift" ,"Mod1" }, "b", function()
          awful.spawn.easy_async_with_shell("killall blender") end),

----[[
    -- Prompt
    awful.key({ modkey },            ";",     function() awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),


--]]

    -- Menubar
    awful.key({ modkey }, "BackSpace", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

clientkeys = gears.table.join(

    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
            end,
        {description = "toggle fullscreen", group = "client"}),

    awful.key({ modkey, "Shift"   }, "c",      function(c) c:kill() end,
       {description = "close", group = "client"}),

    awful.key({ modkey, "Control" }, "f",  awful.client.floating.toggle,
       {description = "toggle floating", group = "client"}),

    awful.key({ modkey, "Shift" }, "f", function() awful.layout.set(awful.layout.suit.floating) end,
       {description = "floating layout", group = "client"}),

    awful.key({ modkey, "Shift" }, "d", function() awful.layout.set(awful.layout.suit.tile) end,
       {description = "tile layout", group = "client"}),

    awful.key({ modkey, "Shift" }, "s", function() awful.layout.set(awful.layout.suit.max) end,
       {description = "max layout", group = "client"}),


    awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
       {description = "move to master", group = "client"}),

    awful.key({ modkey,           }, "o",      function(c) c:move_to_screen()               end,
       {description = "move to screen", group = "client"}),

    awful.key({ modkey,           }, "t",      function(c) c.ontop = not c.ontop            end,
       {description = "toggle keep on top", group = "client"}),

    awful.key({ modkey,           }, "]",
        function(c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),

    awful.key({ modkey,           }, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),

    awful.key({ modkey, "Control" }, "m",
        function(c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),

    awful.key({ modkey, "Shift"   }, "m",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function(c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}


-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },
    { rule = { name = "ghci" },
      properties = fwindow
    },
    { rule = { name = "ranger" },
      properties = fwindow
    },
    { rule = { name = "crt" },
      properties = fwindow
    },
    { rule = { name = "cmus" },
      properties = fwindow
    },
    { rule = { name = "htop" },
      properties = fwindow
    },


--    awful.key({ modkey, "Shift"   }, "Return", function() awful.spawn("cool-retro-term") end),
    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Cadence",
          "Catia",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "qjackctl",
          "Sxiv",
          "Wpa_gui",
          "Pavucontrol",
          "pinentry",
          "Renoise",
          "veromix",
          "xtightvncviewer"},

        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },

    -- Make Firefox not to start maximized
    { rule = { instance = "firefox" },
      properties = { maximized = false }
    },
}
-- }}}

-- {{{ Signals
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

-- Autorun
-- awful.spawn.with_shell("~/.config/awesome/autorun.sh")


client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
