--{-# NOINLINE homeDir #-}

  -- Base
import XMonad hiding ( (|||) )
import XMonad.Config.Desktop
import System.Directory (getHomeDirectory)
import System.IO (hPutStrLn)
--import System.IO.Unsafe (unsafePerformIO)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W
  -- Prompt
import XMonad.Prompt
import XMonad.Prompt.Input
import XMonad.Prompt.Man
import XMonad.Prompt.Pass
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Ssh
import XMonad.Prompt.XMonad
import Control.Arrow (first)
  -- Data
import Data.Char (isSpace, toLower)
import Data.List
import Data.Monoid
--import Data.Maybe (isJust)
import Data.Maybe (isJust, maybeToList)
import Control.Monad (join)
import qualified Data.Map as M
  -- Utilities
import XMonad.Util.PureX
import XMonad.Util.Cursor -- (setDefaultCursor, xC_arrow)
import XMonad.Util.EZConfig (additionalKeysP, additionalMouseBindings)
import XMonad.Util.Scratchpad
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.Loggers
  -- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
--  (dynamiclogwithpp, wrap, xmobarpp, xmobarColor, shorten, PP(..))
import XMonad.Hooks.ManageDocks
  (avoidStruts, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers
--  (doCenterFloat, isDialog, isFullscreen, doFullFloat)
import XMonad.Hooks.SetWMName
--import XMonad.Hooks.ManageDocks
  -- Actions
--import XMonad.Actions.RandomBackground
import qualified XMonad.Actions.FlexibleResize as Flex
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.CopyWindow (kill1, killAllOtherCopies)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.CycleWS
  (moveTo, shiftTo, WSType (..), nextScreen, prevScreen, toggleWS)
import XMonad.Actions.Submap
import XMonad.Actions.GroupNavigation
import XMonad.Actions.Search
  (promptSearch, promptSearchBrowser, duckduckgo, google, hoogle)
  --import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
  -- Layouts modifiers
import XMonad.Layout.Gaps
--import XMonad.Layout.Named
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.Spacing  --  (spacing)
import XMonad.Layout.NoBorders
import XMonad.Layout.LimitWindows
  (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.WindowArranger
  (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.Reflect (REFLECTX(..), REFLECTY(..))
import XMonad.Layout.MultiToggle
  (mkToggle, single, EOT(EOT), Toggle(..), (??))
import XMonad.Layout.MultiToggle.Instances
  (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import qualified XMonad.Layout.ToggleLayouts as T
  (toggleLayouts, ToggleLayout(Toggle))
import XMonad.Layout.LayoutCombinators (JumpToLayout(..), (|||))
--import XMonad.Layout.LayoutCombinators (JumpToLayout(..))
  -- Layouts
--import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.OneBig
import XMonad.Layout.ThreeColumns
import XMonad.Layout.ResizableTile
import XMonad.Layout.ZoomRow
  (zoomRow, zoomReset, ZoomMessage(ZoomFullToggle))

--import qualified Signal as Sig
--------------------------------------------------------------------

-- CONSTANTS
--homedir :: IO String
--homedir = (++) " " <$> (flip (++) "/" <$> getHomeDirectory)
--usr :: IO String
--usr = takeWhile (/= '/') <$> (drop 7 <$> homedir)
--homeDir :: String
--homeDir =
--  case o of
--    "" -> error "Failed to get home directory"
--    _  -> o ++ "/"
--  where o = unsafePerformIO getHomeDirectory
--user :: String
--user = takeWhile (/= '/') $ drop 7 homeDir
homeDir :: String
homeDir = "/home/user1/"

user :: String
user = "user1"

myFont :: String
myFont = "xft:Inconsolata:regular:pixelsize=16"

myModMask :: KeyMask
--myModMask = mod4Mask  -- SuperKey
myModMask = mod1Mask  -- AltKey

altMask :: KeyMask  -- Setting this for use in xprompt
altMask = mod1Mask

myTerminal :: String
myTerminal = "kitty"

mySPTerm :: String
mySPTerm = "cool-retro-term"

qutebrowser = "/bin/qutebrowser"

myBorderWidth :: Dimension
myBorderWidth = 2

myNormColor :: String
myNormColor   = "#242424"

myFocusColor :: String
myFocusColor  = "#a94825"

myGaps :: Int
myGaps = 5

borderRaw = Border 2 2 2 2

windowCount :: X (Maybe String)
windowCount =
  gets $ Just . show . length . W.integrate'
  . W.stack . W.workspace . W.current . windowset
--------------------------------------------------------------------

-- AUTOSTART
--{-
myStartupHook :: X ()
myStartupHook = do
  spawnOnce ("feh --bg-scale /home/user1/.xmonad/glitch13.png")
--  spawnOnce ("sh "
--    ++ "feh --bg-scale "
--    ++ homeDir ++ ".xmonad/glitch13.png")
  setDefaultCursor xC_arrow
  spawnOnce "picom &"
--  spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x292d3e --height 18 &"
--          spawnOnce "emacs --daemon &"
--  setWMName "LG3D"
--}

-------------------------------------------------------------------

-- XPROMPT KEYMAP (emacs-like key bindings)
xPKeymap :: M.Map (KeyMask,KeySym) (XP ())
xPKeymap = M.fromList $
  map (first $ (,) controlMask)
  [ (xK_z, killBefore)
  , (xK_k, killAfter)
  , (xK_a, startOfLine)
  , (xK_e, endOfLine)
  , (xK_m, deleteString Next)
  , (xK_b, moveCursor Prev)
  , (xK_f, moveCursor Next)
  , (xK_BackSpace, killWord Prev)
  , (xK_y, pasteString)
  , (xK_g, quit)
  , (xK_bracketleft, quit)
  ]
  ++
  map (first $ (,) altMask)
  [ (xK_BackSpace, killWord Prev)
  , (xK_f, moveWord Next)
  , (xK_b, moveWord Prev)
  , (xK_d, killWord Next)
  , (xK_n, moveHistory W.focusUp')
  , (xK_p, moveHistory W.focusDown')
  ]
  ++
  map (first $ (,) 0)
  [ (xK_Return, setSuccess True >> setDone True)
  , (xK_KP_Enter, setSuccess True >> setDone True)
  , (xK_BackSpace, deleteString Prev)
  , (xK_Delete, deleteString Next)
  , (xK_Left, moveCursor Prev)
  , (xK_Right, moveCursor Next)
  , (xK_Home, startOfLine)
  , (xK_End, endOfLine)
  , (xK_Down, moveHistory W.focusUp')
  , (xK_Up, moveHistory W.focusDown')
  , (xK_Escape, quit)
  ]
--------------------------------------------------------------------

-- XPROMPT SETTINGS
xPConfig :: XPConfig
xPConfig = def
  { font = "xft:Jet Brains Mono:size=18"
  , bgColor     = "#111111"
  , fgColor     = "#ff7a41"
  , bgHLight    = "#ff7a41"
  , fgHLight    = "#000000"
  , borderColor = "#535974"
  , promptBorderWidth = 0
  , promptKeymap = xPKeymap
  , position = Top
  , height = 33
  , historyFilter = id
  , defaultText = []
  , showCompletionOnTab = False
  , searchPredicate = isPrefixOf
  , alwaysHighlight = True
  , maxComplRows = Nothing
  }
--------------------------------------------------------------------

-- KEYBINDINGS
--{-
myKeys :: [(String, X ())]
myKeys =
  -- Xmonad
  [ ("M-C-r", spawn "xmonad --recompile && xmonad --restart")
  , ("M-S-C-q", io exitSuccess)
  -- Apps
  , ("M-<Return>", spawn myTerminal)
--  , ("M-C-<Return>", randomBg $ HSV 0xff 0x20)
  , ("M-S-<Return>", spawn "cool-retro-term")
  , ("M-u b", spawn "blender")
  , ("M-u e", spawn "emacsclient -nc")
  , ("M-u k", spawn "krita")
  , ("M-u q", spawn qutebrowser)
  , ("M-u r", spawn (myTerminal ++ " -e ranger"))
  , ("M-v", spawn
      (myTerminal
       ++ " -t vifm -e ~/.config/vifm/scripts/vifmrun"))
  , ("M-u w", spawn "firefox")
  -- killall
  , ("M-C-S-h", spawn "killall -9 ghc")
  , ("M-C-S-b", spawn "killall -9 blender")
  -- Prompts
  , ("M-<Backspace>", shellPrompt xPConfig)  -- Shell Prompt
  , ("M-S-o", xmonadPrompt xPConfig)         -- Xmonad Prompt
--  , ("M-S-s", sshPrompt xPConfig)            -- Ssh Prompt
--  , ("M-S-m", manPrompt xPConfig)            -- Manpage Prompt
  -- Windows
  , ("M-S-c", kill1)    -- Kill the currently focused client
--  , ("M-S-a", killAll)  -- Kill all the windows on current workspace
  -- Floating windows
  -- Push floating window back to tile.
  , ("M-t", withFocused $ windows . W.sink)
  -- Push ALL floating windows back to tile.
  , ("M-S-t", sinkAll)
--  , ("M-c")
  -- Windows navigation
  -- Move focus to the master window
  , ("M-m", windows W.focusMaster)
  -- Move focus to the next window
  , ("M-j", windows W.focusDown)
  , ("M-p", windows W.focusDown)
  -- Move focus to the prev window
  , ("M-k", windows W.focusUp)
  , ("M-n", windows W.focusUp)
  -- Swap the focused window and the master window
  , ("M-S-m", windows W.swapMaster)
  -- Swap the focused window with the next window
  , ("M-S-j", windows W.swapDown)
  , ("M-S-p", windows W.swapDown)
  -- Swap the focused window with the prev window
  , ("M-S-k", windows W.swapUp)
  , ("M-S-n", windows W.swapUp)
  -- Moves focused window to master, all others maintain order
--  , ("M-<Backspace>", promote)
  -- Rotate all windows except master and keep focus in place
  , ("M1-S-<Tab>", rotSlavesDown)
  -- Rotate all the windows in the current stack
  , ("M1-C-<Tab>", rotAllDown)
--  , ("M-S-s", windows copyToAll)
--  , ("M-C-s", killAllOtherCopies)
  , ("M-C-M1-<Up>", sendMessage Arrange)
  , ("M-C-M1-<Down>", sendMessage DeArrange)
  --  Move focused window to up
  , ("M-<Up>", sendMessage (MoveUp 10))
  --  Move focused window to down
  , ("M-<Down>", sendMessage (MoveDown 10))
  --  Move focused window to right
  , ("M-<Right>", sendMessage (MoveRight 10))
  --  Move focused window to left
  , ("M-<Left>", sendMessage (MoveLeft 10))
  --  Increase size of focused window up
  , ("M-S-<Up>", sendMessage (IncreaseUp 10))
  --  Increase size of focused window down
  , ("M-S-<Down>", sendMessage (IncreaseDown 10))
  --  Increase size of focused window right
  , ("M-S-<Right>", sendMessage (IncreaseRight 10))
  --  Increase size of focused window left
  , ("M-S-<Left>", sendMessage (IncreaseLeft 10))
  --  Decrease size of focused window up
  , ("M-C-<Up>", sendMessage (DecreaseUp 10))
  --  Decrease size of focused window down
  , ("M-C-<Down>", sendMessage (DecreaseDown 10))
  --  Decrease size of focused window right
  , ("M-C-<Right>", sendMessage (DecreaseRight 10))
  --  Decrease size of focused window left
  , ("M-C-<Left>", sendMessage (DecreaseLeft 10))
  , ("M-<Tab>", nextMatch History (return True))
  -- Layouts
  , ("M-s", sendMessage $ JumpToLayout "tall")
  , ("M-d", sendMessage $ JumpToLayout "monocle")
  , ("M-f", sendMessage $ JumpToLayout "floats")
--  -- Switch to next layout
--  , ("M-<Tab>", sendMessage NextLayout)
  -- Toggles struts
  , ("M-C-z", sendMessage ToggleStruts)
  -- Toggles noborder
  , ("M-S-n", sendMessage $ Toggle NOBORDERS)
  -- Toggles noborder/full
  , ("M-S-=", sendMessage (Toggle NBFULL) >> sendMessage ToggleStruts)
  , ("M-S-f", sendMessage (T.Toggle "float"))
  , ("M-S-x", sendMessage $ Toggle REFLECTX)
  , ("M-S-y", sendMessage $ Toggle REFLECTY)
--  , ("M-S-m", sendMessage $ Toggle MIRROR)
--  -- Increase number of clients in the master pane
--  , ("M-<KP_Multiply>", sendMessage (IncMasterN 1))
--  -- Decrease number of clients in the master pane
--  , ("M-<KP_Divide>", sendMessage (IncMasterN (-1)))
--  -- Increase number of windows that can be shown
--  , ("M-S-<KP_Multiply>", increaseLimit)
--  -- Decrease number of windows that can be shown
--  , ("M-S-<KP_Divide>", decreaseLimit)
  --
  , ("M-h", sendMessage Shrink)
  , ("M-l", sendMessage Expand)
  , ("M-C-j", sendMessage MirrorShrink)
  , ("M-C-k", sendMessage MirrorExpand)
  , ("M-S-;", sendMessage zoomReset)
  , ("M-;", sendMessage ZoomFullToggle)
  -- Workspaces
--  , ("M-.", nextScreen)  -- Switch focus to next monitor
--  , ("M-,", prevScreen)  -- Switch focus to prev monitor
  -- Shifts focused window to next workspace
--  , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)
  -- Shifts focused window to previous workspace
--  , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)
  , ("M-`", toggleWS)
  -- ScratchPadtchpads
  , ("M-u t", scratchpadSpawnActionTerminal mySPTerm)
  , ("M-a a", namedScratchpadAction scratchpads "spcmus")
  , ("M-u g", namedScratchpadAction scratchpads "ghci")
  -- Prompt search
  , ("M-C-s d", promptSearch xPConfig duckduckgo)
  , ("M-C-s g", promptSearch xPConfig google)
  , ("M-C-s h", promptSearch xPConfig hoogle)
  --
  , ("M-S-s d", promptSearchBrowser xPConfig qutebrowser duckduckgo)
  , ("M-S-s g", promptSearchBrowser xPConfig qutebrowser google)
  , ("M-S-s h", promptSearchBrowser xPConfig qutebrowser hoogle)
  -- flameshot
  , ("M-<Print>", spawn "flameshot gui")
  , ("<Print>", spawn
      ("flameshot full -p ~/.screenshots"))
  -- ALSA
  , ("M-<KP_Add>", spawn "amixer set Master 5%+ unmute")
  , ("M-<KP_Subtract>", spawn "amixer set Master 5%- unmute")
  , ("M-<KP_Multiply>", spawn "amixer set Master toggle")
  -- cmus submap
  , ("M-a b", spawn "cmus-remote --next")
  , ("M-a v", spawn "cmus-remote --stop")
  , ("M-a c", spawn "cmus-remote --pause")
  , ("M-a x", spawn "cmus-remote --play")
  , ("M-a z", spawn "cmus-remote --prev")
  -- Lock
  , ("M-<Escape>", spawn "xscreensaver-command --lock")
--  , ("M-S-C z", promote)

  ] ++
  [ (otherModMasks ++ "M-" ++ [key], action tag)
  | (tag, key) <- zip myWorkspaces "1234567890"
  , (otherModMasks, action) <-
    [ ("", windows . W.view), ("S-", windows . W.shift) ]
  ]
  where
    nonNSP = WSIs (return (\ws -> W.tag ws /= "nsp"))
    nonEmptyNonNSP =
      WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))
--}
-- MOUSE
myMouse =
  [ ((mod4Mask, button3), (\w -> focus w >> Flex.mouseResizeWindow w))
--  , ((button1), promote)
--  , ((mod4Mask, button4), (\_ -> windows W.focusUp ))
--  , ((mod4Mask, button5), (\_ -> windows W.focusDown))
  ]
--------------------------------------------------------------------

-- WORKSPACES
xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
  where
    doubleLts '<' = "<<"
    doubleLts x   = [x]

--{-
myWorkspaces =
  [ "일", "이", "삼", "사", "오" , "육", "칠", "팔", "구", "십" ]
--}

{-
myWorkspaces =
  [ "一", "二", "三", "四", "五", "六", "七", "八", "九", "十"]
--}

{-
myWorkspaces :: [String]
myWorkspaces =
  clickable . (map xmobarEscape) $
  [ "일", "이", "삼", "사", "오"
  , "육", "칠", "팔", "구", "십" ]
  where
    clickable l =
      [ "<action=xdotool key super+"
        ++ show (n) ++ ">" ++ ws ++ "</action>"
      | (i,ws) <- zip ([1..9] ++ [0]) l
      , let n = if i == 10 then 0 else i ]
--}

-------------------------------------------------------------------

-- MANAGEHOOK
--{-
myManageHook :: Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     [
--       isFullscreen --> doFullFloat
       isFullscreen       --> (doF W.focusDown <+> doFullFloat)
     , isDialog           --> doFloat
     , namedScratchpadManageHook scratchpads
     , scratchpadManageHook spRect
--     , className =? "vlc" --> doFullFloat
--       (className =? "firefox" <&&> resource =? "Dialog") --> doFloat
     ]
--}
--------------------------------------------------------------------

-- FULLSCREEN SUPPORT
--{-
setFullscreenSupported :: X ()
setFullscreenSupported = addSupported ["_NET_WM_STATE", "_NET_WM_STATE_FULLSCREEN"]
  --
addSupported :: [String] -> X ()
addSupported props = withDisplay $ \dpy -> do
    r <- asks theRoot
    a <- getAtom "_NET_SUPPORTED"
    newSupportedList <- mapM (fmap fromIntegral . getAtom) props
    io $ do
      supportedList <- fmap (join . maybeToList) $ getWindowProperty32 dpy a r
      changeProperty32 dpy r a aTOM propModeReplace (nub $ newSupportedList ++ supportedList)
--}
--------------------------------------------------------------------

-- LAYOUTS
myLayoutHook = smartBorders $ avoidStruts $ mouseResize
  $ windowArrange $ T.toggleLayouts floats
  $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
  where
    myDefaultLayout = tall ||| monocle ||| floats
  --
tall = renamed [Replace "tall"]
  $ limitWindows 12
  $ gaps [(U,myGaps), (D,myGaps), (L,myGaps), (R,myGaps)]
  $ spacingRaw False borderRaw True borderRaw True
  $ ResizableTall 1 (3/100) (1/2) []
monocle = renamed [Replace "monocle"] $ limitWindows 20 Full
floats = renamed [Replace "floats"] $ limitWindows 20 simplestFloat
  --
myLayoutPP :: String -> String
myLayoutPP "tall" =
  "<icon=" ++ homeDir ++ ".xmonad/xpm/icon_layout_tall.xpm/>"
myLayoutPP "monocle" =
  "<icon=" ++ homeDir ++ ".xmonad/xpm/icon_layout_monocle.xpm/>"
myLayoutPP "floats" =
  "<icon=" ++ homeDir ++ ".xmonad/xpm/icon_layout_floats.xpm/>"
myLayoutPP x = x
--------------------------------------------------------------------

-- SCRATCHPADS
spRect :: W.RationalRect
spRect = W.RationalRect l t w h
  where
    (h, w) = (0.9, 0.9)
    (t, l) = (0.95 -h, 0.95 -w)
  --
scratchpads :: [NamedScratchpad]
scratchpads =
  [ NS "spcmus" spawnCmus findCmus manageCmus
  , NS "ghci"   spawnGHCI findGHCI manageGHCI ]
  where
    spawnCmus  = mySPTerm ++  " -T spcmus -e cmus"
    findCmus   = title =? "spcmus"
    manageCmus = manageSPWindow
    spawnGHCI  = mySPTerm ++ " -T ghci -e ghci"
    findGHCI   = title =? "ghci"
    manageGHCI = manageSPWindow
    manageSPWindow :: ManageHook
    manageSPWindow = customFloating spRect
--}
--------------------------------------------------------------------

-- XMOBAR
--{-
formatTitle :: String -> String
formatTitle cs
  | cs             == "spterm"               = "spterm"
  | take 4 cs      == "cmus"                 = "spcmus"
  | cs             == "ghci"                 = "ghci"
  | take 7 cs      == "Blender"              = "blender"
  | t 5 cs         == "Krita"                = "krita"
  | t 15 cs        == "Mozilla Firefox"      = "firefox"
  | t 16cs         == "Renoise (x86_64)"     = "renoise"
  | t 11 cs        == "qutebrowser"          = "qutebrowser"
  | take 5 cs      == "emacs"                = "emacs"
  | t 10 cs        == "VirtualBox"           = "vbox"
  | takeWhile (/= '@') cs == user            = "terminal"
  | cs == "Oracle VM VirtualBox Manager"     = "vbox manager"
--  | otherwise = cs
  | otherwise = tl $ f cs
  where
    tl = map toLower
    t n xs = reverse $ take n $ reverse xs
    f = tl . reverse . takeWhile (/= '-') . reverse
--    f = reverse . takeWhile (/= '-') . reverse
--}

-------------------------------------------------------------------

-- MAIN
main :: IO ()
main = do
  xmproc <- spawnPipe ("xmobar ~/.xmonad/xmobarrc")
--  xmproc <- spawnPipe ("xmobar " ++ homeDir ++ ".xmonad/xmobarrc")
  --
  xmonad $ ewmh desktopConfig
    { manageHook = myManageHook
      <+> manageHook desktopConfig <+> manageDocks

      , modMask            = myModMask
--      , handleEventHook    = fullscreenEventHook
--      , handleEventHook    = setFullscreenSupported
      , terminal           = myTerminal
      , startupHook        = myStartupHook
      , layoutHook         = myLayoutHook
      , workspaces         = myWorkspaces
      , borderWidth        = myBorderWidth
      , normalBorderColor  = myNormColor
      , focusedBorderColor = myFocusColor
      , logHook =
        dynamicLogWithPP xmobarPP
        { ppOutput  = hPutStrLn xmproc
        , ppCurrent = xmobarColor "#EE8800" ""
        , ppLayout  = myLayoutPP
        , ppTitle   = xmobarColor "#EE8800" "" . formatTitle
        , ppUrgent  = xmobarColor "#FF0000" ""
--        , ppExtras = [ logCmd "amixer get Master | tail -n 1" ]
--        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
--        , ppOrder  = \(ws:l:_:ex) -> [ws,l]++ex
--        , ppOrder  = \(ws:l:t:_) -> [ws,l,t]
        }
        <+> historyHook
    }
    `additionalKeysP` myKeys
    `additionalMouseBindings` myMouse


