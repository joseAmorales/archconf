(require 'package)
(require 'image-dired)
(require 'magit)
(add-to-list 'load-path "/usr/share/hindent/elisp")
(require 'hindent)
(add-hook 'haskell-mode-hook #'hindent-mode)
(package-initialize)
(setq inhibit-startup-screen t)
(custom-set-variables
 '(column-number-mode t)
 '(custom-enabled-themes (quote (wombat)))
 '(default-frame-alist (quote ((top . 10)
			       (left . 2)
			       (width . 80)
			       (height . 25))))
 '(display-line-numbers t)
 '(display-line-numbers-grow-only t)
 '(display-line-numbers-type t)
 '(display-line-numbers-width 3)
 '(display-line-numbers-width-start t)
 '(global-display-line-numbers-mode t)
 '(global-whitespace-mode t)
 '(ido-enable-flex-matching nil)
 '(ido-mode (quote both) nil (ido))
 '(initial-scratch-message "¯\\_(ツ)_/¯

")
 '(menu-bar-mode nil)
 '(package-archives
   (quote
    (("melpa-stable" . "http://stable.melpa.org/packages/"))))
 '(package-selected-packages
   (quote
    (fsharp-mode magit multiple-cursors lua-mode smex haskell-mode)))
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(uniquify-buffer-name-style (quote post-forward) nil (uniquify))
 '(whitespace-display-mappings
   (quote
    ((space-mark 32
		 [183]
		 [46])
     (space-mark 160
		 [164]
		 [95])
     (tab-mark 9
	       [187 9]
	       [92 9]))))
 '(whitespace-style
   (quote
    (face trailing tabs spaces indentation::tab
	  indentation::space
	  indentation space-after-tab::tab
	  space-after-tab::space
	  space-after-tab
	  space-before-tab::tab
	  space-before-tab::space
	  space-before-tab space-mark tab-mark))))


(global-visual-line-mode)
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)

(global-set-key (kbd "C-c c") 'comment-region)
(global-set-key (kbd "C-c u") 'uncomment-region)

(global-set-key [C-mouse-4] 'text-scale-increase)
(global-set-key [C-mouse-5] 'text-scale-decrease)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;;; This is yout old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)
(global-set-key (kbd "C-^") 'enlarge-window)
(global-set-key (kbd "C-M-^") 'enlarge-window-horizontally)
(global-set-key (kbd "C-x t") 'eshell)
;;; multiple-cursors
(global-set-key (kbd "C-c m c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
;;; hindent
(global-set-key (kbd "M-q") 'hindent-reformat-decl)
;; magit
(global-set-key (kbd "C-x g") 'magit-status)


(set-frame-font "Inconsolata-14")
(custom-set-faces
 '(default ((t (
		:inherit nil:stipple nil
		:background "#242424"
		:foreground "#f6f3e8"
		:inverse-video nil
		:box nil
		:strike-through nil
		:overline nil
		:underline nil
		:slant normal
		:weight normal
		:width normal
		:family "Inconsolata"))))
 '(border ((t nil)))
 '(cursor ((t (:background "orange"))))
 '(fringe ((t (:background "gray22"))))
 '(header-line ((t (:background "#343434":foreground "#e7f6da"))))
 '(isearch ((t (:background "dim gray" :foreground "light gray"))))
 '(line-number ((t (:inherit (shadow default) :foreground "gray44"))))
 '(line-number-current-line ((t (:inherit line-number :foreground "gray66"))))
 '(mode-line ((t (:background "gray22" :foreground "gray88"))))
 '(mode-line-highlight ((t (:box (:line-width 2 :color "grey40" :style released-button)))))
 '(mode-line-inactive ((t (:background "gray22" :foreground "#857b6f"))))
 '(region ((t (:background "chocolate" :foreground "#f6f3e8"))))
 '(whitespace-big-indent ((t nil)))
 '(whitespace-empty ((t nil)))
 '(whitespace-hspace ((t nil)))
 '(whitespace-indentation ((t (:foreground "gray22"))))
 '(whitespace-line ((t nil)))
 '(whitespace-newline ((t nil)))
 '(whitespace-space ((t (:foreground "gray20"))))
 '(whitespace-space-after-tab ((t nil)))
 '(whitespace-space-before-tab ((t (:foreground "grey77"))))
 '(whitespace-tab ((t (:foreground "grey44"))))
 '(whitespace-trailing ((t (:foreground "grey77")))))
;; Font size
(set-face-attribute 'default nil :height 150)

(add-hook 'dired-mode-hook
      (lambda ()
        (dired-hide-details-mode)))


(defun mp-insert-date ()
  (interactive)
  (insert (format-time-string "%Y-%m-%d")))
(defun mp-insert-time ()
  (interactive)
  (insert (format-time-string "%X")))
(global-set-key (kbd "C-c i d") 'mp-insert-date)
(global-set-key (kbd "C-c i t") 'mp-insert-time)



;; Duplicate line by qmega
(defun duplicate-line-or-region (&optional n)
  "Duplicate current line, or region if active.
With argument N, make N copies.
With negative N, comment out original line and use the absolute value."
  (interactive "*p")
  (let ((use-region (use-region-p)))
    (save-excursion
      (let ((text (if use-region        ;Get region if active, otherwise line
                      (buffer-substring (region-beginning) (region-end))
                    (prog1 (thing-at-point 'line)
                      (end-of-line)
                      (if (< 0 (forward-line 1)) ;Go to beginning of next line, or make a new one
                          (newline))))))
        (dotimes (i (abs (or n 1)))     ;Insert N times, or once if not specified
          (insert text))))
    (if use-region nil                  ;Only if we're working with a line (not a region)
      (let ((pos (- (point) (line-beginning-position)))) ;Save column
        (if (> 0 n)                             ;Comment out original with negative arg
            (comment-region (line-beginning-position) (line-end-position)))
        (forward-line 1)
        (forward-char pos)))))
(global-set-key (kbd "C-S-k") 'duplicate-line-or-region)

