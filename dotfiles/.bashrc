#
# ~/.bashrc
#
#   O  O
#    /\
#  /||||\
#  \||||/
#

#set -o vi # Activate vi mode with <Escape>
HISTSIZE= HISTFILESIZE= # Infinite history
export HISTCONTROL=ignoreboth
export EDITOR=vim
export QT_QPA_PLATFORMTHEME="qt5ct"
export PATH=$PATH:~/.dotnet/tools


# If not running interactively, don't do anything
[[ $- != *i* ]] && return


alias g='grep -i'
alias graph='git log --all --decorate --oneline --graph'
alias l='less -i'
alias la='ls -A'
alias ll='ls -lh'
alias ls='ls -F --color=auto'
alias ps='ps axc | less'
alias ts='date +"%Y-%m-%d_%H-%M-%S"'

alias tma='transmission-remote -tactive -l'
alias tmd='transmission-daemon'
alias tml='transmission-remote -l | less -i'
alias tmr='transmission-remote'


#PS1='[\u@\h \W]\$ '
PS1='\[\033[01;36m\]\u@\h\[\033[00m\] \[\033[01;34m\]\w\[\033[00m\]\n$ '

ponysay --ponyonly
